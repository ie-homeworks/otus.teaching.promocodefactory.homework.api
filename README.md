# Otus.Teaching.PromoCodeFactory

**Мой проект к домашнему заданию "Добавить один из инструментов API к своему проекту"** 

Подробное описание проекта и описание домашних заданий можно найти в [Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Home)
Описание домашнего задания в [Wiki](https://gitlab.com/devgrav/otus.teaching.promocodefactory/-/wikis/Homework-5)

**References**

_**gRPC**_

https://gitlab.com/aa.gerasimenko/Otus.gRPC 

https://github.com/grpc/grpc-dotnet/tree/master/examples

https://learn.microsoft.com/ru-ru/aspnet/core/grpc/protobuf?view=aspnetcore-7.0 

https://protobuf.dev/programming-guides/proto3/ 

https://grpc.io/docs/guides/error/

https://learn.microsoft.com/ru-ru/aspnet/core/grpc/json-transcoding-openapi?view=aspnetcore-7.0


_**SignalR**_

https://github.com/Dzitsky/RealTimeProj1

https://medium.com/swlh/creating-real-time-app-with-asp-net-core-signalr-and-react-in-typescript-90aad9c1170b

